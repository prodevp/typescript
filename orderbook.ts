/**
 * Interfaces for get-all-Orderbook
 */
export interface IOrder {
    readonly amount: string;
    readonly price: string;
    readonly side: "bid" | "ask";
}

export interface IBid {
    readonly side: "bid";
}

export interface IAsk {
    readonly side: "ask";
}

export interface IOrderbookJson {
    readonly bids: ReadonlyArray<IBid>;
    readonly asks: ReadonlyArray<IAsk>;
    readonly timestamp?: string;
}
