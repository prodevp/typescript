import * as async from "async";
import * as csvtojson from "csvtojson";
import * as _ from "lodash";
import * as log from "logger";
import * as moment from "moment";
import * as request from "request";
import * as scientificToDecimal from "scientific-to-decimal";
import {checkEmptyStatus, createHeadersOptions, dataTransforms , outputFormat, toUpper} from "../common/common-functions";
import {docsPath, exchanges, keys} from "../common/constants";
import {httpResponse} from "../common/errors/http-response-errors";
import {HTTP} from "../common/services/request";
import {Iforin, Igatecoin, Iobjn, IParseData, IPublicgetpairs, IPublicpairs, IVircurex} from "../interfaces/response-Interface";
import {transformPairsData} from "./transform-pairs";
const logger = log.createLogger();
const Converter = csvtojson.Converter;

/**
 * Generates the currency pairs
 * Creates an array generated from the requested exchange pairs
 * each exchange of exchanges thru `case`, the corresponding value of
 * each exchange returned by `case`.
 *
 * @Collection
 * @function
 * local: reInitailise
 * other: checkEmptyStatus, createHeadersOptions, outputFormat, toUpper
 * @interface
 * Iforin, Igatecoin, Iobjn, IParseData, IPublicgetpairs, IPublicpairs, IVircurex
 * handle error function: httpResponse
 * @param exchange name of exchange (exchange name should be a single string)
 *
 * @returns {Array} formaatted array
 */
let sepratedPairs: object; // base currency array
let interfaceExchange: object; // contains every exchanges output object
let obj: object; // main object contains final data
let splitCurrency: string[]; // split pairs of currecies
let objInner: object; // Inner object for manipulation
let splitCurrencycheck: string[]; // contains split data
let splitCurrencyIn: string[]; // inner split currency data
let objn: Iobjn; // contains major/minor increament/amount object
let parse: boolean = false;
export function getallCurrencyPairs(exchange: string, raw: number, fileJson: object = null): Promise<string[] | object> {
    raw ? parse = false : parse = true;
    switch (exchange) {
        /**
         * case "csv"
         * Response formatted data from the google csv sheat
         * @augments
         * using variables docsPath and exchange from constants
         * @module
         * using module converter to convert csv to json
         * @returns
         * formatted array
         */
        case "csv":
        return new Promise((resolve, reject) => {
            HTTP("get", docsPath, null, exchange, null, false).then((data) => {
                if (data.length) {
                    raw ? resolve(data) : data = data;
                    // reinitialize the variables holding the values
                    reInitailise();
                    const interfaceExchangecsv: object = {};
                    // convert the csv to json with delimiter comma
                    const converter = new Converter({delimiter: ","});
                    const pairkey: string = 'Currency Pairs (complete this column, only compelete where "Market Data API" is FALSE';
                    const exchangekey: string = "Exchange (auto updates from sheet 1)";
                    const checkBool: string = "Market Data API";
                    // convert csvdata (row colums) to object
                    converter.fromString(data, (error, result) => {
                        if (error) {
                            logger.info(error);
                        }
                        _.mapValues(result, async (csv, key) => {
                            const condition1: boolean = !_.includes(_.trim(csv[exchangekey]), exchanges);
                            const condition2: boolean = csv[checkBool] && csv[checkBool] === "FALSE";
                            const condition = condition1 && condition2 && !_.isEmpty(_.trim(csv[pairkey]));
                            if (condition && _.includes(csv[pairkey], "," || _.includes(csv[pairkey], "-"))) {
                                obj = await transformPairsData(csv, exchange);
                                // assign the formatted result to object
                                _.assign(interfaceExchangecsv, outputFormat(obj, csv["Exchange (auto updates from sheet 1)"]));
                                resolve(interfaceExchangecsv);
                            }
                        });
                    });
                }
            });
        });
        case "bitflyer":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://api.bitflyer.jp/v1/getmarkets", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    obj = await transformPairsData(parseData, exchange);
                    // call to function for formatting of data
                    interfaceExchange = outputFormat(obj, "bitflyer");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "bitstamp":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://www.bitstamp.net/api/v2/trading-pairs-info/", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "bitstamp");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "bitfinex":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://api.bitfinex.com/v1/symbols", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "bitfinex");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "gatecoin":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://api.gatecoin.com/Public/LiveTickers", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    typeof parseData === "string" ? parseData = JSON.parse(parseData) : parseData = parseData;
                    parseData = parseData.tickers;
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "gatecoin");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "exmo":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://api.exmo.com/v1/pair_settings", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "exmo");
                    resolve(interfaceExchange);
                } else {
                        resolve([]);
                }
            });
        });
        case "tidex":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://api.tidex.com/api/3/info", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    parseData = parseData.PAIRS;
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "tidex");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "novaexchange":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://novaexchange.com/remote/v2/markets", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    parseData = parseData.markets
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "novaexchange");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "bittrex":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://bittrex.com/api/v1.1/public/getmarkets", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    parseData = parseData.result
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "bittrex");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "hitbtc":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://api.hitbtc.com/api/2/public/symbol", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    obj = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(obj, "hitbtc");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "cryptopia":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://www.cryptopia.co.nz/api/GetTradePairs", null, exchange, null, parse).then(async (parseData) => {
                typeof parseData === "string" ? parseData = JSON.parse(parseData) : parseData = parseData;
                parseData = parseData.Data;
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    obj = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(obj, "cryptopia");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "btcxindia":
        return new Promise((resolve, reject) => {
            const btcxindia = {
                btcxindia : {
                    XRP: {
                       INR: {},
                    },
                },
            };
            resolve (btcxindia);
        });
        case "therocktrading":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://api.therocktrading.com/v1/funds", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    parseData = parseData.funds;
                    _.mapValues(parseData, (apis: IPublicgetpairs, key) => {
                        if (typeof obj[apis.base_currency] === "undefined") {
                            obj[apis.base_currency] = {};
                            obj[apis.base_currency] = {
                            [apis.trade_currency]: {
                                [keys.minorAmountMin]: scientificToDecimal(apis.minimum_price_offer),
                                [keys.majorAmountMin]: scientificToDecimal(apis.minimum_quantity_offer),
                            },
                            };
                        } else {
                        Object.assign(obj[apis.base_currency], {[apis.trade_currency]: {
                            [keys.minorAmountMin]: scientificToDecimal(apis.minimum_price_offer),
                            [keys.majorAmountMin]: scientificToDecimal(apis.minimum_quantity_offer),
                        }});
                        }
                    });
                    interfaceExchange = outputFormat(obj, "therocktrading");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "cexio":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://cex.io/api/currency_limits", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    typeof parseData === "string" ? parseData = JSON.parse(parseData) : parseData = parseData;
                    parseData = parseData.data.pairs;
                    _.mapValues(parseData, (apis: IPublicgetpairs, key) => {
                        if (typeof obj[apis.symbol1] === "undefined") {
                            obj[apis.symbol1] = {};
                            obj[apis.symbol1] = {
                            [apis.symbol2]: {
                                [keys.minorAmountMax]: scientificToDecimal(apis.maxPrice),
                                [keys.minorAmountMin]: scientificToDecimal(apis.minPrice),
                            },
                            };
                        } else {
                        Object.assign(obj[apis.symbol1], {[apis.symbol2]: {
                            [keys.minorAmountMax]: scientificToDecimal(apis.maxPrice),
                            [keys.minorAmountMin]: scientificToDecimal(apis.minPrice),
                        }});
                        }
                    });
                    interfaceExchange = outputFormat(obj, "cexio");
                    resolve(interfaceExchange);
                } else {
                        resolve([]);
                }
            });
        });
        case "kraken":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://api.kraken.com/0/public/AssetPairs", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    parseData = parseData.result;
                    _.forIn(parseData, (pair, key) => {
                        if (typeof obj[splitCurrency[0]] === "undefined") {
                            obj[splitCurrency[0]] = {};
                            obj[splitCurrency[0]] = {
                                [pair.symbol2]: {
                                [keys.majorIncreament]: scientificToDecimal("1e-" + pair.lot_decimals),
                                [keys.minorIncreament]: "0.001",
                                },
                            };
                        } else {
                            Object.assign(obj[splitCurrency[0]], {[pair.symbol2]: {
                            [keys.majorIncreament]: scientificToDecimal("1e-" + pair.lot_decimals),
                            [keys.minorIncreament]: "0.001",
                            }});
                        }
                    });
                    interfaceExchange = outputFormat(obj, "kraken");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "liqui":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://api.liqui.io/api/3/info", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    parseData  = parseData.PAIRS;
                    _.forIn(parseData, (pair, key) => {
                        splitCurrency = key.split("_");
                        if (typeof obj[splitCurrency[0]] === "undefined") {
                            obj[splitCurrency[0]] = {};
                            obj[splitCurrency[0]] = {
                                [splitCurrency[1]]: {
                                [keys.majorAmountMax] : scientificToDecimal(pair.MAX_PRICE),
                                [keys.majorAmountMin] : scientificToDecimal(pair.MIN_PRICE),
                                [keys.minorAmountMin] : scientificToDecimal(pair.MIN_AMOUNT),
                                [keys.minorAmountMax] : scientificToDecimal(pair.MAX_AMOUNT),
                                },
                            };
                        } else {
                            Object.assign(obj[splitCurrency[0]], {[splitCurrency[1]]: {
                            [keys.majorAmountMax] : scientificToDecimal(pair.MAX_PRICE),
                            [keys.majorAmountMin] : scientificToDecimal(pair.MIN_PRICE),
                            [keys.minorAmountMin] : scientificToDecimal(pair.MIN_AMOUNT),
                            [keys.minorAmountMax] : scientificToDecimal(pair.MAX_AMOUNT),
                            }});
                        }
                    });
                    interfaceExchange = outputFormat(obj, "liqui");
                    resolve(interfaceExchange);
                } else {
                        resolve([]);
                }
            });
        });
        case "tuxexchange":
            return new Promise((resolve, reject) => {
                HTTP("get", "https://tuxexchange.com/api?method=getticker", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "tuxexchange");
                    resolve(interfaceExchange);
                } else {
                        resolve([]);
                }
            });
        });
        case "lykke":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://hft-api.lykke.com/api/AssetPairs", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "lykke");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "gemini":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://api.gemini.com/v1/symbols", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "gemini");
                    resolve(interfaceExchange);
                } else {
                        resolve([]);
                }
            });
        });
        case "bisq":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://markets.bisq.network/api/markets", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "bisq");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "livecoin":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://api.livecoin.net//exchange/ticker", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "livecoin");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "c_cex":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://c-cex.com/t/pairs.json", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    parseData = parseData.PAIRS;
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "c_cex");
                    resolve(interfaceExchange);
                } else {
                        resolve([]);
                }
            });
        });
        case "gate_io":
        return new Promise((resolve, reject) => {
            HTTP("get", "http://data.gate.io/api2/1/pairs", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "gate_io");
                    resolve(interfaceExchange);
                } else {
                        resolve([]);
                }
            });
        });
        case "bitso":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://api.bitso.com/v3/available_books/", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    parseData = parseData.payload;
                    _.mapValues(parseData, (pair, key) => {
                        splitCurrency = pair.book.split("_");
                        if (typeof obj[splitCurrency[0]] === "undefined") {
                            obj[splitCurrency[0]] = {};
                            obj[splitCurrency[0]] = {
                                [splitCurrency[1]]: {
                                [keys.minorAmountMin]: scientificToDecimal(pair.minimum_price),
                                [keys.minorAmountMax]: scientificToDecimal(pair.maximum_price),
                                [keys.majorAmountMin]: scientificToDecimal(pair.minimum_amount),
                                [keys.majorAmountMax]: scientificToDecimal(pair.maximum_amount),
                                },
                            };
                        } else {
                            Object.assign(obj[splitCurrency[0]], {[splitCurrency[1]]: {
                            [keys.minorAmountMin]: scientificToDecimal(pair.minimum_price),
                            [keys.minorAmountMax]: scientificToDecimal(pair.maximum_price),
                            [keys.majorAmountMin]: scientificToDecimal(pair.minimum_amount),
                            [keys.majorAmountMax]: scientificToDecimal(pair.maximum_amount),
                            }});
                        }
                    });
                    interfaceExchange = outputFormat(obj, "bitso");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "vaultoro":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://api.vaultoro.com/markets", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    typeof parseData === "string" ? parseData = JSON.parse(parseData) : parseData = parseData;
                    parseData = parseData.data;
                    obj[parseData.BaseCurrency] = {};
                    objInner[parseData.MarketCurrency] = {};
                    objn[keys.majorAmountMin] = parseData.MinUnitQty.toString();
                    objn[keys.minorAmountMin] = parseData.MinPrice.toString();
                    objn[keys.minorIncreament] = scientificToDecimal(parseData.MinTradeSize);
                    _.assign(objInner[parseData.MarketCurrency], objn);
                    _.assign(obj[parseData.BaseCurrency], objInner);
                    interfaceExchange = outputFormat(obj, "vaultoro");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "quoine":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://api.quoine.com/products", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "quoine");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "lakebtc":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://api.lakebtc.com/api_v2/ticker", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "lakebtc");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "btcturk":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://www.btcturk.com/api/ticker", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "btcturk");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "coingi":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://api.coingi.com/current/24hour-rolling-aggregation", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "coingi");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "dsx_uk":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://dsx.uk/mapi/info", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    parseData = parseData.pairs;
                    _.forIn(parseData, (pair, key) => {
                        splitCurrency = key.toUpperCase().match(/.{1,3}/g);
                        if (typeof obj[splitCurrency[0]] === "undefined") {
                            obj[splitCurrency[0]] = {};
                            obj[splitCurrency[0]] = {
                                [splitCurrency[1]]: {
                                    [keys.minorAmountMin]: scientificToDecimal(pair.min_price),
                                    [keys.minorAmountMax]: scientificToDecimal(pair.max_price),
                                    [keys.majorAmountMin]: scientificToDecimal(pair.min_amount),
                                },
                            };
                        } else {
                            Object.assign(obj[splitCurrency[0]], {[splitCurrency[1]]: {
                                [keys.minorAmountMin]: scientificToDecimal(pair.min_price),
                                [keys.minorAmountMax]: scientificToDecimal(pair.max_price),
                                [keys.majorAmountMin]: scientificToDecimal(pair.min_amount),
                                },
                            });
                        }
                    });
                    interfaceExchange = outputFormat(obj, "dsx_uk");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "southxchange":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://dsx.uk/mapi/info", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "southxchange");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "bleutrade":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://bleutrade.com/api/v2/public/getmarkets", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    parseData = parseData.result;
                    obj = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(obj,  "bleutrade");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "gdax":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://api.gdax.com/products", null, exchange, createHeadersOptions("gdax-node-client"), parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    _.mapValues(parseData, (pair: IPublicgetpairs, key) => {
                        if (typeof obj[pair.base_currency] === "undefined") {
                            obj[pair.base_currency] = {};
                            obj[pair.base_currency] = {
                            [pair.quote_currency]: {
                                [keys.minorIncreament]: pair.quote_increment,
                                [keys.majorAmountMin]: scientificToDecimal(pair.base_min_size),
                                [keys.majorAmountMax]: scientificToDecimal(pair.base_max_size),
                                [keys.minorAmountMin]: scientificToDecimal(pair.min_market_funds),
                                [keys.minorAmountMax]: scientificToDecimal(pair.max_market_funds),
                            },
                            };
                        } else {
                        Object.assign(obj[pair.base_currency], {[pair.quote_currency]: {
                                [keys.minorIncreament]: pair.quote_increment,
                                [keys.majorAmountMin]: scientificToDecimal(pair.base_min_size),
                                [keys.majorAmountMax]: scientificToDecimal(pair.base_max_size),
                                [keys.minorAmountMin]: scientificToDecimal(pair.min_market_funds),
                                [keys.minorAmountMax]: scientificToDecimal(pair.max_market_funds),
                                },
                            });
                        }
                    });
                    interfaceExchange = outputFormat(obj, "gdax");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "zb":
        return new Promise((resolve, reject) => {
            HTTP("get", "http://api.zb.com/data/v1/markets", null, exchange, null, parse).then(async (parseData) => {
                if (Object.keys(parseData).length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "zb");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "bitcoinaverage":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://apiv2.bitcoinaverage.com/constants/symbols", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    typeof parseData === "string" ? parseData = JSON.parse(parseData) : parseData = parseData;
                    parseData = parseData.global.symbols;
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "bitcoinaverage");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "ripple":
        return new Promise((resolve, reject) => {
            const url = "https://data.ripple.com/v2/accounts/rQaxmmRMasgt1edq8pfJKCfbkEiSp5FqXJ/exchanges";
            HTTP("get", url , null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    parseData = parseData.exchanges;
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "ripple");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "vircurex":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://api.vircurex.com/api/get_info_for_currency.json", null, exchange, null, parse).then(async (parseData) => {
                if (Object.keys(parseData).length) {
                    raw ? resolve(parseData) : parseData = parseData;
                    reInitailise();
                    delete parseData.status;
                    obj = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(obj, "vircurex");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "ethexindia":
        return new Promise((resolve, reject) => {
            HTTP("get", "https://api.ethexindia.com/ticker/", null, exchange, createHeadersOptions("ethexindia-node-client"), parse).then(async (parseData) => {
                if (parseData.length) {
                    raw ? resolve([]) : raw = raw;
                    reInitailise();
                    parseData = parseData.ticker;
                    obj = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(obj, "ethexindia");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "yobit":
        return new Promise((resolve, reject) => {
            raw ? resolve([]) : raw = raw;
            HTTP("get", "https://yobit.net/api/3/info", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    parseData = parseData.PAIRS;
                    reInitailise();
                    _.mapValues(parseData, (pair: IPublicgetpairs, key) => {
                        splitCurrency = key.split("_");
                        if (typeof obj[splitCurrency[0]] === "undefined") {
                            obj[splitCurrency[0]] = {};
                            obj[splitCurrency[0]] = {
                                [splitCurrency[1]]: {
                                [keys.majorAmountMin]: scientificToDecimal(pair.MIN_AMOUNT),
                                [keys.minorAmountMin]: scientificToDecimal(pair.MIN_PRICE),
                                [keys.minorAmountMax]: scientificToDecimal(pair.MAX_PRICE),
                                },
                            };
                        } else {
                            Object.assign(obj[splitCurrency[0]], {[splitCurrency[1]]: {
                                [keys.majorAmountMin]: scientificToDecimal(pair.MIN_AMOUNT),
                                [keys.minorAmountMin]: scientificToDecimal(pair.MIN_PRICE),
                                [keys.minorAmountMax]: scientificToDecimal(pair.MAX_PRICE),
                                },
                            });
                        }
                    });
                    interfaceExchange = outputFormat(obj, "yobit");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        case "qryptos":
        return new Promise((resolve, reject) => {
            raw ? resolve([]) : raw = raw;
            HTTP("get", "https://api.qryptos.com/products", null, exchange, null, parse).then(async (parseData) => {
                if (parseData.length) {
                    reInitailise();
                    sepratedPairs = await transformPairsData(parseData, exchange);
                    interfaceExchange = outputFormat(await dataTransforms(sepratedPairs, obj),  "qryptos");
                    resolve(interfaceExchange);
                } else {
                    resolve([]);
                }
            });
        });
        default:
        return new Promise((resolve, reject) => {
            console.log(exchange);
            resolve([]);
        });
    }
}

/**
 * Reinitialize the variables to empty
 * @param
 * void
 * @return
 * void
 */
const reInitailise = () => {
    interfaceExchange = [];
    obj = {};
    splitCurrency = [];
    objInner = [];
    splitCurrencycheck = [];
    splitCurrencyIn = [];
    objn = {};
    sepratedPairs = [];
};
