/**
 * Updates all the pairs.
 * Call the pairs api function parallelly and
 * save the response into a single file
 * @function
 * other: outputFormat, getallCurrencyPairs
 * @argument exchanges array of exchanges name
 */
import * as async from "async";
import * as _ from "lodash";
import * as log from "logger";
import {outputFormat, saveData} from "../common/common-functions";
import {exchanges} from "../common/constants";
import {getallCurrencyPairs} from "./get-all-pairs";
const logger = log.createLogger();
const file = "./public/allexchange/exchange.json";
let exchageName;
let apiData: object[] = [];
// run all the exchanges parallelly
export function updatePairs(download = false, exchangeNames: string[] = []) {
    apiData = [];
    exchangeNames.length === 1 ? exchageName = exchangeNames : exchageName = exchanges;
    return new Promise ((resolve, reject) => {
        async.parallel({
            mainfunctions: (callback) => {
                Promise.all(exchageName).then((values) => {
                    async.forEachOf(exchageName, (exchange: string, key, done) => {
                        getallCurrencyPairs(exchange, 0).then((data) => {
                            if (exchange === "csv") {
                                _.forIn(data, (pair, keyin) => {
                                    logger.info("Success:" + keyin);
                                    apiData.push(outputFormat(pair, keyin));
                                });
                            } else if (data.hasOwnProperty(exchange)) {
                                logger.info("Success:" + exchange);
                                apiData.push(data);
                            }
                            done();
                        }).catch((err) => {
                            return logger.info(err, "error 1" + exchange);
                        });
                    }, (err) => {
                        callback(null, apiData);
                    });
                });
            },
        }, (err, result) => {
            if (result.mainfunctions) {
                if (download) {
                    resolve(result.mainfunctions);
                }
                const formatedData: string = JSON.stringify(result.mainfunctions);
                // write the formatted json string data into the file
                saveData(file, formatedData).then((returned) => {
                    logger.info("Success Please check the file =>" + file);
                });
            }
        });
    });
}
